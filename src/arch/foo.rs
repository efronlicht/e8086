impl Registers {
    /// accumulator high: high byte of [Registers::ax]
    pub fn ah(&mut self) -> &mut u16 { self.h_8_mut(0)}
    /// base high: high byte of [Registers::bx]
    pub fn bh(&mut self) -> &mut u16 { self.h_8_mut(1)}
    /// count high: high byte of [Registers::cx]
    pub fn ch(&mut self) -> &mut u16 { self.h_8_mut(2)}
    /// data high: high byte of [Registers::dx]
    pub fn dh(&mut self) -> &mut u16 {  self.h_8_mut(3)}
    /// accumulator low: high byte of [Registers::ax]
    pub fn al(&mut self) -> &mut u16 {   self.l_8_mut(0) }
    /// base low: low byte of [Registers::bx]
    pub fn bl(&mut self) -> &mut u16 {  self.l_8_mut(1) }
    /// count low: low byte of [Registers::cx]
    pub fn cl(&mut self) -> &mut u16 {  self.l_8_mut(2) }
    /// data low: low byte of [Registers::dx]
    pub fn dl(&mut self) -> &mut u16 {  self.l_8_mut(3)}
}
