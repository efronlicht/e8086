pub mod flags {
    use bitflags::bitflags;

    #[derive(Clone, Copy, PartialEq, Eq, Debug, Default)]
    pub struct Bools {
        pub cf: bool,
        pub pf: bool,
        pub acf: bool,
        pub zf: bool,
        pub sf: bool,
        pub tf: bool,
        pub if_: bool,
        pub df: bool,
        pub of: bool,
    }

    /// ##### `T`, `I`, and `D` are the control flags, which change the function of the processor.
    pub const CONTROL_FLAGS: Bits = Bits {
        bits: TF.bits | IF.bits | DF.bits,
    };
    impl Bools {
        pub const EMPTY: Self = Self {
            cf: false,
            pf: false,
            acf: false,
            zf: false,
            sf: false,
            tf: false,
            df: false,
            of: false,
            if_: false,
        };

        pub const fn from_bits(f: Bits) -> Self {
            Self {
                cf: f.contains(CF),
                pf: f.contains(PF),
                acf: f.contains(ACF),
                zf: f.contains(ZF),
                sf: f.contains(SF),
                tf: f.contains(TF),
                df: f.contains(DF),
                of: f.contains(OF),
                if_: f.contains(IF),
            }
        }
    }

    /// Carry/Borrow. Status Flag. Set if last operation carried (for +, *, <<) or borrowed (-, /, >>)
    pub const CF: Bits = Bits::CF;
    /// Parity. Status Flag. Set if last arithmetic op is EVEN, otherwise cleared.
    pub const PF: Bits = Bits::CF;
    ///  Auxilary Carry. Status Flag (BCD only): Set if Binary Coded Decimal operations generate a carry.
    pub const ACF: Bits = Bits::CF;
    ///  Zero. Status Flag: set if  last arithmetic or logical op gives zero, otherwise cleared.
    pub const ZF: Bits = Bits::CF;
    ///  Sign. Status Flag: set if last arithmetic operation negative, otherwise cleared.
    pub const SF: Bits = Bits::CF;
    ///  Trap. Control Flag. If set(1), generate an interrupt after each instruction. (Used for single-step debugging.)
    pub const TF: Bits = Bits::CF;
    ///  Interrupt Enable. Control Flag. If set (1), allow maskable interrupts (i.e, from peripherals). Else, disable.
    pub const IF: Bits = Bits::CF;
    // Direction. Control Flag. If set (1), access HIGH-to-LOW. Else (0), acess LOW-TO-HIGH.
    pub const DF: Bits = Bits::CF;
    /// Overflow. Status Flag. Set if last operation overflowed.
    pub const OF: Bits = Bits::OF;
    ///  `CY`, `P`, `AC`, `Z`, `S`, and `O` are the status flags, set after arithmetic or logical operations.
    pub const STATUS_FLAGS: Bits = Bits {
        bits: CF.bits | PF.bits | ACF.bits | ZF.bits | SF.bits | OF.bits,
    };
    bitflags! {
        /// Flag register is 16 bits. Nine are used:
        /// 6 are Status Flags, set after operations:
        ///     Sign 'S', Zero 'Z', Auxilary Carry 'AC', Parity 'P', Carry/Borrow 'CY', and Overflow 'O'.
        /// 3 are Control Flags, which change the function of the processor.
        ///     Trap 'T', Direction 'D', Interrupt Enable 'I'.
        #[repr(transparent)]
        pub struct Bits: u16 {
    /// Carry/Borrow Status Flag. Set if last operation carried (for +, *, <<) or borrowed (-, /, >>)
    const CF = 1 << 0;
            /// Parity Status Flag. Set if last arithmetic op is EVEN, otherwise cleared.
            const PF = 1 << 2;
            ///  Auxilary Carry Status Flag (BCD only): Set if Binary Coded Decimal operations generate a carry.
            const ACF = 1 << 4;
            ///  Zero. Status Flag: set if  last arithmetic or logical op gives zero, otherwise cleared.
            const ZF = 1 << 6  ;
            ///  Sign. Status Flag: set if last arithmetic operation negative, otherwise cleared.
            const SF = 1 << 7 ;
            ///  Trap. Control Flag. If set(1), generate an interrupt after each instruction. (Used for single-step debugging.)
            const TF = 1 << 8;
            /// ##### Interrupt Enable. Control Flag. If set (1), allow maskable interrupts (i.e, from peripherals). Else, disable.
            const IF = 1 << 9;
            /// ##### Direction. Control Flag. If set (1), access HIGH-to-LOW. Else (0), acess LOW-TO-HIGH.
            const DF = 1 << 10;
            /// ##### Overflow. Status Flag. Set if last operation overflowed.
            const OF = 1 << 11;

        }
    }
}

//// [CPU] registers
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub struct Registers(pub [u16; 12]);

// helper functions for raw memory manipulation
impl Registers {
    #[inline(always)]
    pub fn as_mut_16(&mut self) -> &mut [u16; 12] { &mut self.0 }

    #[inline(always)]
    pub fn as_8(&self) -> &[u8; 24] { unsafe { std::mem::transmute(&self.0) } }

    #[inline(always)]
    pub fn as_mut_8(&mut self) -> &mut [u8; 24] { unsafe { std::mem::transmute(&mut self.0) } }

    /// high byte of the register at position i.
    #[inline(always)]
    pub fn h_8_mut(&mut self, i: usize) -> &mut u8 {
        debug_assert!(i < 24);
        #[cfg(target_endian = "little")]
        unsafe {
            self.as_mut_8().get_unchecked_mut(i * 2 + 1)
        }
        #[cfg(target_endian = "big")]
        unsafe {
            self.as_8().get_unchecked_mut(i * 2)
        }
    }

    /// high byte of the register at position i.
    #[inline(always)]
    pub fn h_8(&self, i: usize) -> u8 {
        debug_assert!(i < 24);
        #[cfg(target_endian = "little")]
        unsafe {
            *self.as_8().get_unchecked(i * 2 + 1)
        }
        #[cfg(target_endian = "big")]
        unsafe {
            *self.as_8().get_unchecked_mut(i * 2)
        }
    }

    /// high byte of the register at position i.
    #[inline(always)]
    pub fn l_8_mut(&mut self, i: usize) -> &mut u8 {
        debug_assert!(i < 24);
        #[cfg(target_endian = "little")]
        unsafe {
            self.as_mut_8().get_unchecked_mut(i * 2)
        }
        #[cfg(target_endian = "big")]
        unsafe {
            self.as_8().get_unchecked_mut(i * 2 + 1)
        }
    }

    /// low byte of the register at position i.
    #[inline(always)]
    pub fn l_8(&self, i: usize) -> u8 {
        debug_assert!(i < 24);

        #[cfg(target_endian = "little")]
        unsafe {
            *self.as_8().get_unchecked(i * 2)
        }
        #[cfg(target_endian = "big")]
        unsafe {
            *self.as_8().get_unchecked(i * 2 + 1)
        }
    }

    #[inline(always)]
    pub fn u_16(&self, i: usize) -> u16 {
        debug_assert!(i < 12);
        *unsafe { self.0.get_unchecked(i) }
    }

    #[inline(always)]
    pub fn i_16(&mut self, i: usize) -> i16 {
        debug_assert!(i < 12);
        *unsafe { self.as_mut_16().get_unchecked(i) } as i16
    }

    #[inline(always)]
    /// raw access to the ith register, interpreted as an i16
    pub fn i_16_mut(&mut self, i: usize) -> &mut i16 {
        debug_assert!(i < 12);
        unsafe { std::mem::transmute(self.as_mut_16().get_unchecked_mut(i)) }
    }

    #[inline(always)]
    /// raw access to the ith register, interpreted as an u16
    pub fn u_16_mut(&mut self, i: usize) -> &mut u16 {
        debug_assert!(i < 12);
        unsafe { self.as_mut_16().get_unchecked_mut(i) }
    }
}
impl Registers {
    /// accumulator: gp register for most arithmetic and logical ops
    #[inline]
    pub fn ax(&mut self) -> &mut u16 { self.u_16_mut(0) }

    /// base register: addresses
    #[inline]
    pub fn bx(&mut self) -> &mut u16 { self.u_16_mut(1) }

    /// count register: usually loops
    #[inline]
    pub fn cx(&mut self) -> &mut u16 { self.u_16_mut(2) }

    /// data register: (mul, div)
    #[inline]
    pub fn dx(&mut self) -> &mut u16 { self.u_16_mut(3) }

    /// code segment: base location of program code
    #[inline]
    pub fn cs(&mut self) -> &mut u16 { self.u_16_mut(4) }

    /// data segment: base location for variables
    #[inline]
    pub fn ds(&mut self) -> &mut u16 { self.u_16_mut(5) }

    /// stack segment: base of stack
    #[inline]
    pub fn ss(&mut self) -> &mut u16 { self.u_16_mut(6) }

    /// extra segment
    #[inline]
    pub fn es(&mut self) -> &mut u16 { self.u_16_mut(7) }

    /// base pointter: offset from stack segment register
    #[inline]
    pub fn bp(&mut self) -> &mut u16 { self.u_16_mut(8) }

    /// stack pointer: offset from ss register where the stack's TOP is located.
    #[inline]
    pub fn sp(&mut self) -> &mut u16 { self.u_16_mut(9) }

    /// source index register: src for strcpy, among others
    #[inline]
    pub fn si(&mut self) -> &mut u16 { self.u_16_mut(10) }

    /// destination index register: dst for strcpy, among others
    #[inline]
    pub fn di(&mut self) -> &mut u16 { self.u_16_mut(11) }
}
// high-low access to gp registers
impl Registers {
    /// accumulator high: high byte of [Registers::ax]
    #[inline]
    pub fn ah(&mut self) -> &mut u8 { self.h_8_mut(0) }

    /// base high: high byte of [Registers::bx]
    #[inline]
    pub fn bh(&mut self) -> &mut u8 { self.h_8_mut(1) }

    /// count high: high byte of [Registers::cx]
    #[inline]
    pub fn ch(&mut self) -> &mut u8 { self.h_8_mut(2) }

    /// data high: high byte of [Registers::dx]
    #[inline]
    pub fn dh(&mut self) -> &mut u8 { self.h_8_mut(3) }

    /// accumulator low: high byte of [Registers::ax]
    #[inline]
    pub fn al(&mut self) -> &mut u8 { self.l_8_mut(0) }

    /// base low: low byte of [Registers::bx]
    #[inline]
    pub fn bl(&mut self) -> &mut u8 { self.l_8_mut(1) }

    /// count low: low byte of [Registers::cx]
    #[inline]
    pub fn cl(&mut self) -> &mut u8 { self.l_8_mut(2) }

    /// data low: low byte of [Registers::dx]
    #[inline]
    pub fn dl(&mut self) -> &mut u8 { self.l_8_mut(3) }
}
#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub struct CPU {
    /// instruction pointer.
    pub ip: u16,
    /// Status [Flags] set after operations. Sign, Zero, Auxilary Carry, Parity, Carry/Borrow, and Overflow.
    pub flags: flags::Bits,
    /// control flags
    /// accumulator used for most arithmetic operations.
    pub registers: Registers,
}
impl Registers {
    pub const fn new() -> Self { Self([0; 12]) }
}
pub struct Machine {
    pub cpu: CPU,
    /// an 8086 can access up to 1024*1024 bytes.
    pub mem: Vec<u8>,
}

pub trait Clock {
    fn next(&mut self) -> bool;
}
use crate::asm::{ASM, RMI};
impl Machine {
    pub fn init() -> Self { todo!() }

    pub fn new_raw() -> Self { Self::with_cap(1024 * 1024) }

    /// - read a machine instruction
    ///     - use the IP to look up where we are in memory.
    ///     - convert the first byte at that address to an opcode.
    ///     - lookup how many more instructions that opcode needs.
    ///     - read those off the stack, incrementing IP as needed
    /// - convert the whole pile into assembly
    ///     - run the ASM op, remembering to properly set the flags
    ///     - something something interrupts?
    /// - do the time warp again
    pub fn step(self) -> Result<Self, usize> {
        // computers are easy, right?
        self.cpu.ip;
        todo!()
    }

    pub fn run_asm(&mut self, asm: ASM) {}

    pub fn with_cap(cap: usize) -> Self {
        assert!(cap <= 1024 * 1024, "8086 can't address more than 1 MiB");
        Self {
            cpu: CPU {
                ip: 0,
                flags: flags::Bits::empty(),
                registers: Registers::new(),
            },
            mem: vec![0; cap],
        }
    }
}
