use crate::{
    arch::{self, flags::Bools, *},
    asm::{self, *},
};
fn exec(mut instruction: ASM, mut m: Machine) -> arch::Machine {
    let unchanged = arch::flags::Bools::from_bits(m.cpu.flags);
    let arch::flags::Bools {
        cf,
        pf,
        acf,
        zf,
        sf,
        tf,
        df,
        of,
        if_,
    } = unchanged;
    let mut jmp = |pos, m: &mut Machine| {
        assert!(pos as usize <= m.mem.len(), "{instruction:?}: jump outside range of memory!");
        unchanged // jumps never change flags
    };
    let dir = if df { -1 } else { 1 };
    use ASM::*;

    let flags = match instruction {
        AAA | AAD | AAM | DAA | DAS => unimplemented!("BCD instructions"),
        // Add with carry.
        ADC(dst, src) => {
            let src = src.val(&m);
            let dst = dst.addr(&mut m);

            let (res, cf) = dst.carrying_add(src, cf);
            *dst = res;
            flags::Bools {
                cf,
                of: cf, // picked up from carry
                zf: res == 0,
                sf: res & 0x80 != 0,
                pf: res & 1 != 0, // normal status flags
                acf: false,       // only true for BCD ops,
                ..unchanged
            }
        }
        // Add (no carry)
        ADD(rm, rmi) => {
            let (res, cf) = rm.val(&m).overflowing_add(rmi.val(&mut m));
            *rm.addr(&mut m) = res;
            flags::Bools {
                cf,
                of: cf, // picked up from carry
                zf: res == 0,
                sf: res & 0x80 != 0,
                pf: res & 1 != 0, // normal status flags
                acf: false,       // only true for BCD ops
                ..unchanged
            }
        }
        // Logical AND.
        AND(dst, src) => {
            let res = src.val(&m) & dst.val(&m);
            *dst.addr(&mut m) = res;
            flags::Bools {
                zf: res == 0,
                sf: res & 0x80 != 0,
                pf: res & 1 != 0, // normal status flags
                acf: false,       // only true for BCD ops
                ..unchanged
            }
        }
        // Call procedure
        CALL(M) => todo!("CALL"),
        // Convert byte to word.
        CBW => todo!("CBW"),
        CLC => flags::Bools { cf: false, ..unchanged },
        // Clear Carry Flag.
        CLD => flags::Bools { df: false, ..unchanged },
        CLI => flags::Bools { if_: false, ..unchanged },
        CMC => flags::Bools { cf: !cf, ..unchanged },
        // Compare by subtracting OP1 from OP2, then setting the [arch::Registers] status flags.
        CMP(op1, op2) => {
            let (res, borrow) = op2.val(&m).overflowing_sub(op1.val(&m));
            flags::Bools {
                cf: borrow,
                of: borrow,
                zf: res == 0,
                sf: res & 0x80 != 0,
                pf: res & 1 != 0, // normal status flags
                acf: false,       // only true for BCD ops
                ..unchanged
            }
        }

        /// Compare bytes ES:[DI] from DS[:SI] (TODO: seems complicated.)
        CMPSB | CMPSW => todo!(),
        CWD => todo!(),
        DEC(rm) => {
            let (res, cf) = rm.val(&m).overflowing_sub(1);
            *rm.addr(&mut m) = res;
            flags::Bools {
                cf,
                of: cf, // picked up from carry
                zf: res == 0,
                sf: res & 0x80 != 0,
                pf: res & 1 != 0, // normal status flags
                acf: false,       // only true for BCD ops
                ..unchanged
            }
        }

        DIV(RM::R(R::AL | R::BL | R::CL | R::DL | R::AH | R::BH | R::CH | R::DH)) => {
            todo!("8-bit unsigned div")
        }
        DIV(rm) => todo!("16-bit unsigned div"),
        HLT => panic!("HLT!"),
        IDIV(RM::R(R::AL | R::BL | R::CL | R::DL | R::AH | R::BH | R::CH | R::DH)) => {
            todo!("8-bit signed siv")
        }
        IDIV(rm) => todo!("16-bit signed div"),
        IN(io) | OUT(io) => todo!("IO"),
        INC(rm) => {
            let (res, cf) = rm.val(&m).overflowing_add(1);
            *rm.addr(&mut m) = res;
            flags::Bools {
                cf,
                of: cf, // picked up from carry
                zf: res == 0,
                sf: res & 0x80 != 0,
                pf: res & 1 != 0, // normal status flags
                acf: false,       // only true for BCD ops
                ..unchanged
            }
        }
        IMUL(RM::R(R::AL | R::BL | R::CL | R::DL | R::AH | R::BH | R::CH | R::DH)) => {
            todo!("8-bit signed mul")
        }
        IMUL(rm) => todo!("16-bit signed mul"),

        INT(u8) => todo!("unconditional interrupt"),
        INTO => todo!("interrupt if overflow"),
        IRET => todo!("interrupt return!"),
        // conditional jumps: cond true
        JA(pos) if cf || zf => jmp(pos, &mut m),             // above
        JAE(pos) if !cf => jmp(pos, &mut m),                 // above equal
        JB(pos) if todo!() => jmp(pos, &mut m),              // below
        JBE(pos) if cf || of => jmp(pos, &mut m),            // below or equal
        JC(pos) if cf => jmp(pos, &mut m),                   // carry
        JCXZ(pos) if R::CX.val(&m) == 0 => jmp(pos, &mut m), // cx zero
        JE(pos) if zf => jmp(pos, &mut m),                   // equal
        JG(pos) if todo!() => jmp(pos, &mut m),              // greater
        JGE(pos) if cf == of => jmp(pos, &mut m),            // greater or equal
        JL(pos) if cf != of => jmp(pos, &mut m),             // lower
        JLE(pos) if cf == of || zf => jmp(pos, &mut m),      // lower or equal
        JMP(pos) => jmp(pos, &mut m),                        // unconditional
        JNA(pos) if cf || zf => jmp(pos, &mut m),            // not above
        JNAE(pos) if cf => jmp(pos, &mut m),                 // not above or equal
        JNB(pos) if !cf => jmp(pos, &mut m),                 // not below
        JNBE(pos) if !cf && !zf => jmp(pos, &mut m),         // not below or equal
        JNC(pos) if !cf => jmp(pos, &mut m),                 // no carry
        JNE(pos) if !zf => jmp(pos, &mut m),                 // not equal
        JNG(pos) if zf && (sf != of) => jmp(pos, &mut m),    // no greater
        JNGE(pos) if sf == of => jmp(pos, &mut m),           // no greater or equal
        JNL(pos) if sf != of => jmp(pos, &mut m),            // no lower
        JNLE(pos) if !(zf && (sf != of)) => jmp(pos, &mut m), // no lower or equal
        JNO(pos) if !of => jmp(pos, &mut m),                 // no overflow
        JNP(pos) if !pf => jmp(pos, &mut m),                 // no parity (odd)
        JNS(pos) if !sf => jmp(pos, &mut m),                 // not signed
        JNZ(pos) if !zf => jmp(pos, &mut m),                 // not zero
        JO(pos) if of => jmp(pos, &mut m),                   // overflow
        JP(pos) if pf => jmp(pos, &mut m),                   // parity (even)
        JPO(pos) if !pf => jmp(pos, &mut m),                 // parity odd
        JS(pos) if sf => jmp(pos, &mut m),                   // signed
        JZ(pos) if zf => jmp(pos, &mut m),                   // zero
        // conditional jumps: cond false
        JA(pos) | JAE(pos) | JB(pos) | JBE(pos) | JC(pos) //  0
        |JCXZ(pos) | JE(pos) | JG(pos) | JGE(pos) | JL(pos) // 1 
        | JLE(pos) | JNA(pos) | JNAE(pos) | JNB(pos) | JNBE(pos) // 2
        | JNC(pos) | JNE(pos) | JNG(pos) | JNL(pos) | JNLE(pos) // 3 
        | JNO(pos) | JNP(pos) | JNS(pos) | JNZ(pos) | JO(pos) | JP(pos) // 4  
        | JPO(pos) | JS(pos) | JZ(pos) =>  {
            assert!(pos as usize <= m.mem.len(), "{instruction:?}: jump outside range of memory!");
            unchanged
        },
        LAHF => todo!(),
        /// Load u32 to R and DS.
        LDS(r) => todo!(),
        // Load effective offset.
        LEA(r, (segment, offset)) => todo!(),
        /// Load u32 to R (first word) and DS (2nd word)
        LES(r, (segment, offset)) => todo!(),
        LODSB | LODSW => todo!(),
        LOOP(pos) => {
            let cx = m.cpu.registers.cx();
            *cx -= 1;
            if *cx != 0 {
                jmp(pos, &mut m);
            }
            unchanged
        }
        LOOPE(pos) | LOOPZ(pos) => {
            let cx = m.cpu.registers.cx();
            *cx -= 1;
            if *cx != 0 && zf {
                jmp(pos, &mut m);
            }
            unchanged
        }
        LOOPNE(pos) | LOOPNZ(pos) => {
            let cx = m.cpu.registers.cx();
            *cx -= 1;
            if *cx != 0 && !zf {
                jmp(pos, &mut m);
            }
            unchanged
        }
        MOV(rm, rmi) => {
            let n = rmi.val(&m);
            *rm.addr(&mut m) = n;
            unchanged
        }
        MOVSB | MOVSW => todo!(),
        MUL(RM::R(R::AL | R::BL | R::CL | R::DL | R::AH | R::BH | R::CH | R::DH)) => {
            todo!("8-bit unsigned mul")
        }
        MUL(rm) => todo!("16-bit unsigned mul"),
        NEG(rm) => {
            let (res, cf) = (!rm.val(&m)).overflowing_add(1);
            *rm.addr(&mut m) = res;
            flags::Bools {
                tf,
                df,
                if_, // unchanged
                cf,
                of: cf, // picked up from carry
                zf: res == 0,
                sf: res & 0x80 != 0,
                pf: res & 1 != 0, // normal status flags
                acf: false,       // only true for BCD ops
            }
        }
        //     MOVSB,
        //     /// Copy word at DS:[SI] to ES:[DI]. Update SI and
        //     MOVSW,
        //     /// Multiply '*' (unsigned)
        //     ///
        //     /// 16: (DX AX) = AX*op
        //     /// 8: AX = AL*op
        //     MUL(RM),
        //     /// Negate, two's complement. (invert bits and add 1). Signed.
        //     NEG(RM),
        //     /// NOP does nothing (but still takes a cycle).
        //     NOP,
        //     /// Logical Not. '`!`' (or `^`, for you C fans.) Bitwise invert.
        //     NOT(RM),
        //     /// Logical Or. '|' Result is stored in first operand.
        //     OR(RM, RMI),
        //     /// Output to specified port. See [IO]
        //     OUT(IO),
        //     /// Get 16-bit value from the stack. Note that this increments the stack pointer by TWO.
        //     POP(RMI),
        //     #[deprecated = "80816 CPU and later only"]
        //     /// Pop all general purpose registers DI, SI, BP, _SP_ (ignored), BX, DX, CX, AX from the stack.
        //     POPA,
        //     /// Push a 16 bit value onto the stack.
        //     PUSH(RMI),
        //     #[deprecated = "80816 CPU and later only"]
        //     PUSHA,
        //     /// Store flags on the stack.
        //     PUSHF,
        //     /// Rotate through Carry Left. See [note on rotations](ASM). Rotate left THROUGH the carry. That is, the leftmost bit becomes the new carry,
        //     /// and the old carry bit becomes the rightmost bit. See also [ROL]
        //     RCL(RM),
        //     /// Rotate through Carry Right. See [RCL], [note on rotations](ASM) for details, [ROR] for non-carry.
        //     RCR(RM),
        //     /// Repeat an instruction CX times. Valid instructions:
        //     ///
        //     /// [MOVSB], [MOVSW], [LODSB], [LODSW], [STOSB], [STOSW].
        //     REP,
        //     /// Repeat While Equal (ZF == 1), up to CX times. Valid instructions:
        //     ///  [CMPSB], [CMPSW], [SCASB], [SCASW]
        //     REPE,
        //     /// Repeat While Not Equal. As [REPE], but while ZF == 0.
        //     REPNE,
        //     /// Repeat While NonZero. As [REPNZ]. ATTENTION: identical?
        //     REPNZ,
        //     /// Repeat While Zero. As [REPE].
        //     REPZ,
        //     /// Return from near prodecure. (Wait, it's all GOTOs? Always has been.)
        //     RET,
        //     /// Return from far procedure.
        //     RETF,
        //     /// Rotate left. The bit that goes off the left reappears on the right AND in the carry. See also [ROL] and [ASM].
        //     ROL(RM),
        //     /// Rotate right. The bit that goes off the right reappears on the left AND in the carry. See also [ROL] and [ASM].
        //     ROR(RM),
        //     /// Shift Arithmetic Left, (sign-preserving?) filling the right with zeroes. See [RCL], [note on rotations](ASM). Sets CF, OF.
        //     SAL(RM),
        //     /// Shift Arithmetic Right (SIGN-PRESERVING). See [RCL], [note on rotations](ASM).
        //     SAR(RM),
        //     /// SuBtract with Borrow.
        //     SBB(RM, RMI),
        //     /// Compare bytes: AL from ES:DI. /// TODO: explain this better when I understand it.
        //     SCASB,
        //     /// Compare bytes: AX from ES:DI. /// TODO: explain this better when I understand it.
        //     SCASW,
        //     /// SHift Left (logical). NOT sign-preserving. CF is shifted-off bit.
        //     ///
        //     /// Flags: CF = leftmost bit. OF = op keeps orignal sign
        //     SHL(RM),
        //     /// SHift Right (logical). As [SHL].
        //     SHR(RM),
        //     /// Set Carry [Flags]: DF=1.
        //     STC,
        //     /// Set Direction [Flags]: DF=1.
        //     STD,
        //     /// Set Interrupt Enable [Flags]: IF=1.
        //     STI,
        //     /// Store Segment Byte. Store byte in AL into ES:\[DI\]. Update DI (by 1).
        //     STOSB,
        //     /// Store Segment Word. Store word in AX into ES:DI. Update DI (by 2).
        //     STOWD,
        //     /// SUBtract (without borrow: see [SBB]).
        //     SUB(RM, RMI),
        //     /// Logical AND between all bits to set `ZF`, `SF`, `PF`.
        //     TEST(RM, RMI),
        //     /// eXCHanGe values of operands, like [std::mem::swap]
        //     XCHG(RM, RMI),
        //     /// Translate byte from table. Copy value of memory byte at
        //     /// DS:\[BX + unsigned AL\] to AL register.
        //     XLATB,
        //     /// Logical XOR, storing the results in the first operand.
        //     XOR(RM, RMI),
        _ => todo!(),
    };
    m
}
