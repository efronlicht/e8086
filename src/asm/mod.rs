use super::arch::Registers;
/// See [Registers]
#[derive(Clone, Copy, PartialEq, Eq, Debug, PartialOrd, Ord)]
#[repr(u8)]
pub enum Register {
    /// Register: 16 bit. Accumulator, Base Register, Count Register, Data Register.
    AX = 0x00,
    BX = 0x01,
    CX = 0x02,
    DX = 0x03,
    /// Registers: 8 bit (low bytes). Offset is in the bottom two bits.
    AL = 0x20,
    BL = 0x21,
    CL = 0x22,
    DL = 0x23,
    /// Registers: 8 bit (high byte). Offset is in the bottom two bits.
    AH = 0x10,
    BH = 0x11,
    CH = 0x12,
    DH = 0x13,

    /// Code Segment, Data Segment, Stack Segment, Extra Segment
    CS = 0x04,
    DS = 0x05,
    SS = 0x06,
    ES = 0x07,
    /// Base Pointer, Stack Pointer, Source Index, Destination Index
    BP = 0x08,
    SP = 0x09,
    SI = 0x0A,
    DI = 0x0B,
}

pub type R = Register;
pub type M = (u16, u16);
/// Label. May change.
/// FIXME: figure this out.
/// how do I translate between labels and addresses?
/// do I just give labels a number and then use a translation table?
/// what does an assembler really do, anyways?
pub type L = u32;
#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord)]
/// A [R]egister, [M]emory location, or [I]mmediate value.
pub enum RMI {
    R(R),
    M(M),
    I(I),
}
/// [I] immediate value operand.
type I = u8;
#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord)]
/// A [R]egister or [M]emory location.
pub enum RM {
    /// [R]egister
    R(R),
    /// [M]emory location
    M(M),
}

use ASM::*;

use crate::{
    arch,
    arch::{flags::Bits, Machine},
};
#[non_exhaustive]
#[derive(Clone, Copy, PartialEq, Eq, Debug)]
/// NOTE ON ROTATIONS:
/// 8086 ASSEMBLY has instructions to rotate or shift multiples at a time,
/// but the machine can only rotate or shift one at a time.
/// I haven't yet decided how to handle this.
/// RIGHTS NOTE: some of these instructions are taken VERBATIM from
/// http://www.gabrielececchetti.it/Teaching/CalcolatoriElettronici/Docs/i8086_instruction_set.pdf.
/// I will attempt to get permission. If I can't find it, or a license, I will try and rewrite
/// those bits in my own words before release. If I forget, THANK YOU for your excellent documentation,
/// and hopefully you don't mind too much.
pub enum ASM {
    /// Adjust after addition. BCD only.
    AAA,
    /// Adjust after division. BCD only.
    AAD,
    /// Adjust after multiplication. BCD only.
    AAM,
    /// Adjust after subtraction. BCD only.
    AAS,
    /// Add with carry
    ADC(RM, RMI),
    /// Add (no carry)
    ADD(RM, RMI),
    /// Logical AND (&)
    AND(RM, RMI),
    /// Call procedure. TODO: is this right?
    CALL(M),
    /// Convert byte into word.
    CBW,
    /// Clear Carry Flag.
    CLC,
    /// Clear Direction Flag.
    CLD,
    /// Clear Interrupt Enable Flag, disabling hardware interrupts.
    CLI,
    /// Complement (invert) Carry Flag.
    CMC,
    /// Compare by subtracting OP1 from OP2, then setting the [arch::Registers] status flags.
    CMP(RMI, RM),
    /// Compare bytes ES:[DI] from DS[:SI] (TODO: seems complicated.)
    CMPSB,
    /// Compare words: ES:[DI] from DS:[SI].  (TODO: seems complicated.)
    CMPSW,
    /// Convert Word to Double.
    CWD,
    /// Decimal Adjust After Addition. BCD only.
    DAA,
    /// Decimal Adjust After Subtraction. BCD only.
    DAS,
    /// Decrement
    DEC(RM),
    /// Divide (unsigned).
    ///
    /// 16: AX = (DX*AX) / op, DX = (DX*AX) % op. H/L: AL = A
    ///
    /// 8:  AH = AX / op; AL = AX % op.
    DIV(RM),
    /// Halt (and catch fire)
    HLT,
    /// Signed Divide.
    ///
    /// 16: AX = (DX*AX) / op, DX = (DX*AX) % op. H/L: AL = A
    ///
    /// 8:  AH = AX / op; AL = AX % op.
    IDIV(RM),
    /// Signed Multiply.
    ///
    /// 16: (DX AX) = AX*op
    /// 8: AX = AL*op
    IMUL(RM),
    /// INPUT from port into AL or AX. See [IO] for details.
    IN(IO),
    /// Increment
    INC(RM),
    /// Interrupt
    INT(u8),
    /// Interrupt if overflow.
    INTO,
    /// Interrupt Return. Pop from stack: ip, cs, flags.
    IRET,
    // conditional jumps
    /// Jump if Above (CF == 0 && ZF == 0). Unsigned.
    JA(L),
    /// Jump if Above Equal. (unsigned) (CF == 0)
    JAE(L),
    /// Jump if Below (CF == 1)
    JB(L),
    /// Jump if Below (or) equal. Unsigned.
    JBE(L),
    /// Jump if Carry.(CF == 1) (duplicate of JB?)
    JC(L),
    /// Jump if CX Zero (CX 0)
    JCXZ(L),
    // Jump Equal (ZF == 0)
    JE(L),
    /// Jump if greater (signed)
    JG(L),
    /// Jump if greater than or equal (SF == OF). Signed.
    JGE(L),
    /// Jump if less than. (SF != OF). Signed.
    JL(L),
    /// Jump if less than or equal. (SF != OF || ZF == 1)
    JLE(L),
    /// unconditional jump.
    JMP(L),
    /// Jump not above (CF == 1 || ZF == 1). Unsigned.
    JNA(L),
    /// Jump not above and not equal. (CF == 1). Unsigned.
    JNAE(L),
    /// Jump not below. (CF == 0). Unsigned.
    JNB(L),
    /// Jump not below and not equal. (CF == 0 && ZF == 0). Unsigned.
    JNBE(L),
    /// Jump No Carry. (CF == 0)
    JNC(L),
    /// Jump not equal. Signed & Unsigned
    JNE(L),
    /// Jump not greater. (ZF == 1 && SF != OF). Signed.
    JNG(L),
    /// Jump not greater equal. (ZF != OF). Signed.
    JNGE(L),
    /// Jump not less (SF == OF). Signed.
    JNL(L),
    /// Jump not less,  not equal. (SF == OF && ZF == 0). Signed.
    JNLE(L),
    /// Jump no overflow.
    JNO(L),
    /// Jump no parity (odd). (PF = 0).
    JNP(L),
    /// Jump not signed (SF == 0).
    JNS(L),
    /// Jump not zero. (ZF != 0)
    JNZ(L),
    /// Jump if overflow. (OF = 1)
    JO(L),
    /// Jump Parity (PF = 1)
    JP(L),
    /// Jump Parity Even(??) // isn't this the same as JP?
    JPE(L),
    /// Jump Parity Odd (??) // isn't this the same as JO?
    JPO(L),
    // Jump Signed. (SF == 1)
    JS(L),
    /// Jump Zero. (ZF==1)
    JZ(L),
    /// Load AH from 8 low bits of flags register.
    /// SF ZF 0 AF 0 PF 1 CF
    LAHF,
    /// Load memory double S. First word goes to R, second to DS.
    LDS(R),
    /// Load effective offset.
    LEA(R, M),
    /// Load memory double & ES. First word to R, second to ES.
    LES(R, M),
    /// Load byte at DS:SI into AL, then update SI according to DF.
    LODSB,
    /// Load byte at DS:SI into AX, then update SI according to DF.
    LODSW,
    /// Decrease CX, then jump if CX not zero. (CF != 0)
    LOOP(L),
    /// Decrease CX, jump if CX not zero and equal. (JUMP: CF != 0 && ZF == 0)
    LOOPE(L), // TODO: from LOOPE onwards
    /// Decrease CX, jump if CX not zer oand not equal (JUMP: CF != 0 &&  ZF  == 0)
    LOOPNE(L),
    /// Decrease CX, jump to label if CX not zero and ZF == 0
    LOOPNZ(L),
    LOOPZ(L),
    MOV(RM, RMI),
    /// Copy byte at DS:[SI] to ES:[DI]. Update SI and DI.
    MOVSB,
    /// Copy word at DS:[SI] to ES:[DI]. Update SI and
    MOVSW,
    /// Multiply '*' (unsigned)
    ///
    /// 16: (DX AX) = AX*op
    /// 8: AX = AL*op
    MUL(RM),
    /// Negate, two's complement. (invert bits and add 1). Signed.
    NEG(RM),
    /// NOP does nothing (but still takes a cycle).
    NOP,
    /// Logical Not. '`!`' (or `^`, for you C fans.) Bitwise invert.
    NOT(RM),
    /// Logical Or. '|' Result is stored in first operand.
    OR(RM, RMI),
    /// Output to specified port. See [IO]
    OUT(IO),
    /// Get 16-bit value from the stack. Note that this increments the stack pointer by TWO.
    POP(RMI),
    #[deprecated = "80816 CPU and later only"]
    /// Pop all general purpose registers DI, SI, BP, _SP_ (ignored), BX, DX, CX, AX from the stack.
    POPA,
    /// Push a 16 bit value onto the stack.
    PUSH(RMI),
    #[deprecated = "80816 CPU and later only"]
    PUSHA,
    /// Store flags on the stack.
    PUSHF,
    /// Rotate through Carry Left. See [note on rotations](ASM). Rotate left THROUGH the carry. That is, the leftmost bit becomes the new carry,
    /// and the old carry bit becomes the rightmost bit. See also [ROL]
    RCL(RM),
    /// Rotate through Carry Right. See [RCL], [note on rotations](ASM) for details, [ROR] for non-carry.
    RCR(RM),
    /// Repeat an instruction CX times. Valid instructions:
    ///
    /// [MOVSB], [MOVSW], [LODSB], [LODSW], [STOSB], [STOSW].
    REP,
    /// Repeat While Equal (ZF == 1), up to CX times. Valid instructions:
    ///  [CMPSB], [CMPSW], [SCASB], [SCASW]
    REPE,
    /// Repeat While Not Equal. As [REPE], but while ZF == 0.
    REPNE,
    /// Repeat While NonZero. As [REPNZ]. ATTENTION: identical?
    REPNZ,
    /// Repeat While Zero. As [REPE].
    REPZ,
    /// Return from near prodecure. (Wait, it's all GOTOs? Always has been.)
    RET,
    /// Return from far procedure.
    RETF,
    /// Rotate left. The bit that goes off the left reappears on the right AND in the carry. See also [ROL] and [ASM].
    ROL(RM),
    /// Rotate right. The bit that goes off the right reappears on the left AND in the carry. See also [ROL] and [ASM].
    ROR(RM),
    /// Shift Arithmetic Left, (sign-preserving?) filling the right with zeroes. See [RCL], [note on rotations](ASM). Sets CF, OF.
    SAL(RM),
    /// Shift Arithmetic Right (SIGN-PRESERVING). See [RCL], [note on rotations](ASM).
    SAR(RM),
    /// SuBtract with Borrow.
    SBB(RM, RMI),
    /// Compare bytes: AL from ES:DI. /// TODO: explain this better when I understand it.
    SCASB,
    /// Compare bytes: AX from ES:DI. /// TODO: explain this better when I understand it.
    SCASW,
    /// SHift Left (logical). NOT sign-preserving. CF is shifted-off bit.
    ///
    /// Flags: CF = leftmost bit. OF = op keeps orignal sign
    SHL(RM),
    /// SHift Right (logical). As [SHL].
    SHR(RM),
    /// Set Carry [Flags]: DF=1.
    STC,
    /// Set Direction [Flags]: DF=1.
    STD,
    /// Set Interrupt Enable [Flags]: IF=1.
    STI,
    /// Store Segment Byte. Store byte in AL into ES:\[DI\]. Update DI (by 1).
    STOSB,
    /// Store Segment Word. Store word in AX into ES:DI. Update DI (by 2).
    STOWD,
    /// SUBtract (without borrow: see [SBB]).
    SUB(RM, RMI),
    /// Logical AND between all bits to set `ZF`, `SF`, `PF`.
    TEST(RM, RMI),
    /// eXCHanGe values of operands, like [std::mem::swap]
    XCHG(RM, RMI),
    /// Translate byte from table. Copy value of memory byte at
    /// DS:\[BX + unsigned AL\] to AL register.
    XLATB,
    /// Logical XOR, storing the results in the first operand.
    XOR(RM, RMI),
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub enum IO {
    AL_DX,
    AL_I,
    AX_DX,
    AX_I,
}

/// variable-length machine instruction. length is 1..=6. second argument is always the length.
/// [Deref] to a slice of length 1..=6
struct MachineInstruction([u8; 6], u8);
impl Deref for MachineInstruction {
    type Target = [u8];

    fn deref(&self) -> &[u8] {
        debug_assert!(self.1 > 0 && self.1 <= 6);
        &self.0[..self.1 as usize]
    }
}

struct MachineParser<I: Iterator<Item = u8>>(I);
fn count_ops(opcode: u8) -> Result<u8, Error> {
    match opcode {
        _ => Err(Error::UnimplementedOpcode),
        _ => Err(Error::InvalidOpcode),
    }
}

impl<I: Iterator<Item = u8>> Iterator for MachineParser<I> {
    type Item = Result<MachineInstruction, Error>;

    fn next(&mut self) -> Option<Self::Item> {
        let opcode = self.0.next()?;
        match count_ops(opcode) {
            Err(err) => Some(Err(err)),
            Ok(len @ 1..=6) => {
                let mut b = [opcode, 0, 0, 0, 0, 0];
                for n in b.iter_mut().take(len as usize).skip(1) {
                    if let Some(inst) = self.0.next() {
                        *n = inst;
                    } else {
                        return Some(Err(Error::NotEnoughInstructions));
                    }
                }
                Some(Ok(MachineInstruction(b, len)))
            }
            Ok(len) => unreachable!("opcodes must have 1..=6 instructions, but had {len}"),
        }
    }
}

impl std::str::FromStr for ASM {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> { todo!() }
}
impl From<ASM> for MachineInstruction {
    fn from(asm: ASM) -> Self { ASM::to_machine(asm) }
}

/// An operand that can be obtained as a 16-bit value. That is, [RMI] or it's subvariants.
pub trait ValOp {
    /// value of the operand as a u16.
    fn val(self, m: &Machine) -> u16;
}
impl ValOp for u16 {
    fn val(self, _: &Machine) -> u16 { self }
}
impl ValOp for u8 {
    fn val(self, _: &Machine) -> u16 { self as u16 }
}
impl ValOp for R {
    #[inline]
    fn val(self, m: &Machine) -> u16 {
        let i = self as usize;
        match self {
            R::AH | R::BH | R::CH | R::DH => m.cpu.registers.h_8(i & 0b11) as u16,
            R::AL | R::BL | R::CL | R::DL => m.cpu.registers.l_8(i & 0b11) as u16,
            _ => m.cpu.registers.u_16(i),
        }
    }
}
impl ValOp for RMI {
    #[inline]
    fn val(self, m: &Machine) -> u16 {
        match self {
            RMI::I(n) => n as u16,
            RMI::M(mem) => mem.val(m),
            RMI::R(reg) => reg.val(m),
        }
    }
}
impl ValOp for RM {
    fn val(self, m: &Machine) -> u16 {
        match self {
            RM::R(r) => r.val(m),
            RM::M(mem) => mem.val(m),
        }
    }
}

const fn mem_addr((segment, offset): (u16, u16)) -> usize {
    (segment as usize) << 4 + (offset as usize)
}
impl ValOp for (u16, u16) {
    fn val(self, m: &Machine) -> u16 {
        let i = mem_addr(self);
        /// TODO: make sure memory ordering correct OK here. I think it should be BIG-endian.
        let (high, low) = (u16::from(m.mem[i]), u16::from(m.mem[i + 1]));
        high << 4 | low
    }
}

impl AddrOp for R {
    #[inline]
    fn addr8(self, m: &mut Machine) -> &mut u8 {
        match self {
            _ => unimplemented!("can't take 8-bit address of anything but AH..=DH or AL..=DL"),
            // we store the register offset in the low two bits.
            R::AH | R::BH | R::CH | R::DH => m.cpu.registers.h_8_mut(self as usize & 0b11),
            R::AL | R::BL | R::CL | R::DL => m.cpu.registers.l_8_mut(self as usize & 0b11),
        }
    }

    #[inline]
    fn addr(self, m: &mut Machine) -> &mut u16 {
        match self {
            R::AH | R::BH | R::CH | R::DH | R::AL | R::BL | R::CL | R::DL => unimplemented!(),
            // safety: the maximum value is 0xB0.
            i => m.cpu.registers.u_16_mut(i as usize),
        }
    }
}
impl AddrOp for M {
    #[inline]
    fn addr8(self, m: &mut Machine) -> &mut u8 { &mut m.mem[mem_addr(self)] }

    #[cfg(target_endian = "little")]
    /// TODO: figure out alignment / endian-ness.
    /// I don't think this transmute is safe!
    fn addr(self, m: &mut Machine) -> &mut u16 { unsafe { std::mem::transmute(self.addr8(m)) } }
}
impl AddrOp for RM {
    #[inline]
    fn addr8(self, m: &mut Machine) -> &mut u8 {
        match self {
            RM::R(r) => r.addr8(m),
            RM::M(mem) => mem.addr8(m),
        }
    }

    #[inline]
    fn addr(self, m: &mut Machine) -> &mut u16 {
        match self {
            RM::R(r) => r.addr(m),
            RM::M(mem) => mem.addr(m),
        }
    }
}

/// An operand that can be addressed and modified (i.e, a register or memory location).
/// addr and addr8 modes are mutually exclusive and will panic.
pub trait AddrOp: ValOp + Copy {
    /// get the register or
    fn addr(self, m: &mut Machine) -> &mut u16;
    fn addr8(self, m: &mut Machine) -> &mut u8;
    #[inline]
    fn addr_i(self, m: &mut Machine) -> &mut i16 { unsafe { std::mem::transmute(self.addr(m)) } }
    #[inline]
    fn addr_i8(self, m: &mut Machine) -> &mut u8 { unsafe { std::mem::transmute(self.addr8(m)) } }
    fn set(self, m: &mut Machine, val: u16) { *self.addr(m) = val }
}
impl From<MachineInstruction> for ASM {
    fn from(mi: MachineInstruction) -> Self { ASM::from_machine(mi) }
}
use std::{io, ops::Deref};
impl ASM {
    /// raw byte operation
    const fn opcode(&self) -> u8 {
        match self {
            _ => unimplemented!(),
        }
    }

    const fn name(&self) -> &'static str {
        match self {
            _ => todo!(),
        }
    }

    const fn from_raw_bytes(b: &[u8]) -> Result<Self, Error> { todo!() }

    const fn to_machine(self) -> MachineInstruction { todo!() }

    const fn from_machine(m: MachineInstruction) -> ASM { todo!() }

    fn disassemble(it: impl IntoIterator<Item = u8>) -> Result<Vec<Self>, Error> {
        todo!("this probably needs a symbol table or something")
    }

    fn assemble_vec(it: impl IntoIterator<Item = Self>) -> Vec<u8> {
        let mut v = Vec::new();
        let _ = Self::assemble(it, &mut v); // can't fail
        v
    }

    fn assemble(it: impl IntoIterator<Item = Self>, w: &mut impl io::Write) -> io::Result<usize> {
        let mut sum = 0;
        for asm in it {
            sum += w.write(&asm.to_machine())?;
        }
        Ok(sum)
    }
}
#[non_exhaustive]
#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum Error {
    NotEnoughInstructions,
    InvalidOpcode,
    UnimplementedOpcode,
}
